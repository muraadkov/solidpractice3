package main.java.domain;

public class PHPDeveloper implements BackEndDeveloper {
    private String firstName;
    private String lastName;
    private String birthdayDate;
    private int salary;
    private int bonuses;

    PHPDeveloper(){
        bonuses = 0;
    }

    public PHPDeveloper(String firstName, String lastName, String birthdayDate, int salary){
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthdayDate = birthdayDate;
        this.salary = salary;
    }


    @Override
    public void backEnd() {
        System.out.println("My name is: " + firstName + ", I am write PHP code");
    }

    public void develop() {
        this.backEnd();
    }

    public void work() {
        this.develop();
    }
}
