package main.java.domain;

public interface BackEndDeveloper extends Developer  {
    void backEnd();
}
