package main.java.domain;

public class Main {
    public static void main(String[] args) {
        Company company = new Company();

        company.addEmployee();
        company.startWork();
    }
}
