package main.java.domain;

public interface Developer extends Employee{
    public void develop();
}
