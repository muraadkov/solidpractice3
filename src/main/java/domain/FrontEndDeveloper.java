package main.java.domain;

public interface FrontEndDeveloper extends Developer {
    void frontEnd();
}
