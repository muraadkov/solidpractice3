package main.java.domain;

public class HTMLDeveloper implements FrontEndDeveloper {
    private String firstName;
    private String lastName;
    private String birthdayDate;
    private int salary;
    private int bonuses;

    public HTMLDeveloper(){
        bonuses = 0;
    }

    public HTMLDeveloper(String firstName, String lastName, String birthdayDate, int salary){
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthdayDate = birthdayDate;
        this.salary = salary;
    }

    @Override
    public void frontEnd() {
        System.out.println("My name is: " + firstName + ", I am create HTML page");
    }
    public void develop() {
        this.frontEnd();
    }

    public void work() {
        this.develop();
    }

}
